# Final project CoinMonkey

Group Members:
1) Lucero Aguilar
2) Durga Chapagai
3) Arun Shrish

Description:
This is a game where a monkey will collect a Black Coin that appears and disappear randomly and it will increase its points. If it touches the moving ellipses, it will lose the game.  The game uses sound, minim and we have used .png image. We use mp3 for the background music.  Moreover, the monkey collects its points the more will it be its score. 

How to play:
As the monkey moves, select arrow keys up, down, arrow key to get the coins above it. When you jump above the coin, it will collect the points. More the points, more the score.

How to run:
Just simply click on sketch file and run the program, then up and down button will direct where it needs to be while collecting the coin.
