//Importing sound from library
import processing.sound.*;
SoundFile file;

//Getting ready to load images
PImage img;

//Declaring variables for monkey
int x;
int y=226; 
boolean play = true;

//Initializing variable to move circles
int xPos = 150;
int yPos = 150;

//Setting up canvas
void setup(){
  size(950, 450);
   background(0);
  
//Adding Monkey image  
img = loadImage("Monkey.png");
//img = loadImage("Banana.png");

//Adding background music
file = new SoundFile(this, "Carefree-Days-in-Groovyville.mp3");
file.loop();
}

//Drawing in the canvas (naming my sets)
void draw(){
  
  wall();
  bubbles();
  monkey();
  ball();
  ball2();
coins();
  //banana();
}

//color of wall (blue)
void wall() {                 
  fill(100, 126, 400);
  rect(0,88,width,200);
  noStroke();
}

//code for all the colored circles
void bubbles() {              
  color c[] = new color[3];
  int n=0;
  c[0]=color(400, 400, 0);
  c[1]=color(0, 255, 255);
  c[2]=color(500, 0, 255); 
  for(int i=-2; i<width; i+=25){
    strokeWeight(2);
    stroke(0);
    fill(c[n]);
    ellipse(i,88,35,35); //top circles
    ellipse(i,70,35,35); //top circles
    ellipse(i,52,35,35); //top circles
    ellipse(i,34,35,35); //top circles
    ellipse(i,16,35,35); //top circles
    ellipse(i,-2,35,35); //tip circles
    ellipse(i,300,35,35); //bottom circles
    ellipse(i,318,35,35); //bottom circles
    ellipse(i,336,35,35); //bottom circles
    n++;
    if(n>2) n=0;
  }
}

//Monkey image/moving
void monkey(){                       
 image(img, x+120, y-37,150,100);
  if (play){
    x++;}
  if(x>width){
  x=0;}}
void keyPressed() {
  if(key == CODED) {
    if(keyCode == UP) {
      if(y == 226){
      y-= 100;}
    }
  }
   if(keyCode == DOWN){
      if(y == 126){
      y+= 100;}
    }
    if(keyCode == ' '){
      play = !play;}
  }
  
  //Adding bananas
// void banana(){
//image(img, x, y, 100, 100);
//  }

//All balls floating (left to right)
 void ball(){
 fill(155);
  ellipse(xPos, 170, 30, 30);
  ellipse(xPos, 120, 30, 30 );
  xPos -=5;
  if(xPos < -10){
    xPos = 850;}}
    
 void ball2(){
   fill(155);
   ellipse(yPos, 170, 30, 30);
   ellipse(yPos, 130, 30, 30);
   yPos -=2;
   if(yPos < -4){
     yPos = 900;}}
void coins (){
  fill (10);
  color(400, 800, 400);
  ellipse(170, 170, 25, 25);
  ellipse(600, 170, 25, 25);
  ellipse(75, 150, 25, 25);
ellipse(400, 200, 20, 20 );
ellipse(160, 250, 20, 20);
ellipse( 700, 250, 20, 20);
ellipse (800, 175, 20, 20);
}
